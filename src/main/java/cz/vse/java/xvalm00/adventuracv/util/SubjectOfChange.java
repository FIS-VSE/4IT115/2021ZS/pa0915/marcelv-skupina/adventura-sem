package cz.vse.java.xvalm00.adventuracv.util;

public interface SubjectOfChange {

    /**
     * Registrace pozorovatele změn
     * @param observer pozorovatel
     */
    void registerObserver(Observer observer);

    /**
     * Odebrání ze seznamu pozorovatelů
     * @param observer pozorovatel
     */
    void unregisterObserver(Observer observer);

    /**
     * Upozornění registrovaných pozorovatelů na změnu
     */
    void notifyObservers();

}

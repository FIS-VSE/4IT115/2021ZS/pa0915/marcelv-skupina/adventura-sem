package cz.vse.java.xvalm00.adventuracv.main;

import cz.vse.java.xvalm00.adventuracv.gui.HerniPlocha;
import cz.vse.java.xvalm00.adventuracv.gui.PanelBatohu;
import cz.vse.java.xvalm00.adventuracv.gui.PanelVychodu;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.uiText.TextoveRozhrani;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class AdventuraZaklad extends Application {

    private final double karkulkaRychlost = 300.0; // pixels per second
    private final double vlkRychlost = 100.0; // pixels per second

    private ImageView viewKarkulky =  new ImageView(new Image(HerniPlocha.class.getResourceAsStream("/zdroje/karkulka.png"), 30.0, 40.0, false, true));
    private Image srdickoImage = new Image(HerniPlocha.class.getResourceAsStream("/zdroje/srdce.png"), 20.0, 25.0, false, true);
    private ImageView viewSrdce1 =  new ImageView(srdickoImage);
    private ImageView viewSrdce2 =  new ImageView(srdickoImage);
    private ImageView viewSrdce3 =  new ImageView(srdickoImage);
    private ImageView viewSrdce4 =  new ImageView(srdickoImage);
    private ImageView viewVlk =  new ImageView(new Image(HerniPlocha.class.getResourceAsStream("/zdroje/vlk.png"), 100.0, 70.0, false, true));

    private IHra hra = Hra.getSingleton();
    private TextField prikazovePole;
    private Label zadejPrikazLabel;
    private HBox spodniBox;
    private TextArea konzole;
    private BorderPane hraBorderPane = new BorderPane();
    private VBox menuAHraVBox = new VBox();
    private MenuBar menuBar;
    private HerniPlocha mapaHry;
    private PanelVychodu panelVychodu;
    private PanelBatohu panelBatohu;

    public static void main(String[] args) {
        if (args.length == 0) {
            launch(args);
        } else {
            if (args[0].equals("-text")) {
                IHra hra = Hra.getSingleton();
                TextoveRozhrani ui = new TextoveRozhrani(hra);
                ui.hraj();
                System.exit(0);
            } else if (args[0].equals("-gui")) {
                launch(args);
            }
            else {
                System.out.println("Byl zadan neplatny parametr.");
            }
        }
    }

    @Override
    public void start(Stage primaryStage) {
        mapaHry = new HerniPlocha(hra.getHerniPlan(), viewKarkulky);
        AnchorPane mapaHryAnchorPane = mapaHry.getAnchorPane();

        pridejSrdceAVlka(mapaHryAnchorPane);

        panelVychodu = new PanelVychodu(hra.getHerniPlan());
        ListView<String> listView = panelVychodu.getListView();

        panelBatohu = new PanelBatohu(hra);

        pripravKonzoli();
        pripravPrikazovePole();
        pripravSpodniPanel();
        pripravHraBorderPane(mapaHryAnchorPane, listView, panelBatohu);
        pripravMenu();

        menuAHraVBox.getChildren().addAll(menuBar, hraBorderPane);
        pripravScenuAStage(primaryStage);
        prikazovePole.requestFocus();
    }

    private void pridejSrdceAVlka(AnchorPane mapaHryAnchorPane) {
        mapaHryAnchorPane.getChildren().addAll(viewSrdce1, viewVlk);

        viewVlk.setBlendMode(BlendMode.COLOR_BURN);

        AnchorPane.setLeftAnchor(viewVlk, 150.0);
        AnchorPane.setTopAnchor(viewVlk, 150.0);

        AnchorPane.setLeftAnchor(viewSrdce1, 200.0);
        AnchorPane.setTopAnchor(viewSrdce1, 200.0);
    }

    private void animuj(Scene scene) {
        LongProperty lastUpdateTime = new SimpleLongProperty();
        DoubleProperty karkulkyPohybX = new SimpleDoubleProperty();
        DoubleProperty karkulkyPohybY = new SimpleDoubleProperty();


        AnimationTimer karkulkaAnimation = new AnimationTimer() {
            @Override
            public void handle(long timestamp) {
                if (lastUpdateTime.get() > 0) {
                    double elapsedSeconds = (timestamp - lastUpdateTime.get()) / 1_000_000_000.0 ;

                    double deltaX = elapsedSeconds * karkulkyPohybX.get();
                    double oldX = viewKarkulky.getTranslateX();

                    double deltaY = elapsedSeconds * karkulkyPohybY.get();
                    double oldY = viewKarkulky.getTranslateY();

                    viewKarkulky.setTranslateX(oldX + deltaX);
                    viewKarkulky.setTranslateY(oldY + deltaY);
                }
                lastUpdateTime.set(timestamp);
            }
        };
        karkulkaAnimation.start();


        scene.setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case A:
                    karkulkyPohybX.set(-karkulkaRychlost);
                    break;
                case D:
                    karkulkyPohybX.set(karkulkaRychlost);
                    break;
                case S:
                    karkulkyPohybY.set(karkulkaRychlost);
                    break;
                case W:
                    karkulkyPohybY.set(-karkulkaRychlost);
                    break;
            }

        });

        scene.setOnKeyReleased(event -> {
            karkulkyPohybX.set(0);
            karkulkyPohybY.set(0);
        });
    }

    private void pripravMenu() {
        Menu souborMenu = new Menu("Soubor");

        // nova hra
        ImageView novaHraIkonka = new ImageView(new Image(AdventuraZaklad.class.getResourceAsStream("/zdroje/new.gif")));
        MenuItem novaHra = new MenuItem("Nova hra", novaHraIkonka);
        novaHra.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));
        novaHra.setOnAction(event -> {
            hra = Hra.restartHry();
            konzole.setText(hra.vratUvitani());
            hra.getHerniPlan().registerObserver(mapaHry);
            hra.getHerniPlan().registerObserver(panelVychodu);
            hra.getBatoh().registerObserver(panelBatohu);
            mapaHry.novaHra(hra.getHerniPlan());
            panelVychodu.novaHra(hra.getHerniPlan());
            panelBatohu.novaHra(hra.getBatoh());
            prikazovePole.requestFocus();
        });

        SeparatorMenuItem separator = new SeparatorMenuItem();

        MenuItem konec = new MenuItem("Konec");
        konec.setOnAction(event -> System.exit(0));

        souborMenu.getItems().addAll(novaHra, separator, konec);

        Menu napovedaMenu = new Menu("Nápověda");
        MenuItem oAplikaci = new MenuItem("O aplikaci");
        MenuItem napovedaKAplikaci = new MenuItem("Nápověda k aplikaci");
        napovedaMenu.getItems().addAll(oAplikaci, napovedaKAplikaci);

        oAplikaci.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Graficka adventura");
            alert.setHeaderText("JavaFX adventura");
            alert.setContentText("verze LS 2020");
            alert.showAndWait();
        });

        napovedaKAplikaci.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Napoveda k aplikaci");
            WebView webView = new WebView();
            webView.getEngine().load(AdventuraZaklad.class.getResource("/zdroje/napoveda.html").toExternalForm());
            stage.setScene(new Scene(webView, 600, 600));
            stage.show();
        });

        menuBar = new MenuBar();

        menuBar.getMenus().addAll(souborMenu, napovedaMenu);
    }

    private void pripravScenuAStage(Stage primaryStage) {
        Scene scene = new Scene(menuAHraVBox, 600, 450);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Adventura");
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            Platform.exit();
            System.exit(0);
        });

        animuj(scene);
    }

    private void pripravHraBorderPane(AnchorPane planekHry, ListView<String> listView, PanelBatohu panelBatohu) {
        hraBorderPane.setTop(planekHry);
        hraBorderPane.setRight(listView);
        hraBorderPane.setCenter(konzole);
        hraBorderPane.setLeft(panelBatohu.getPanel());
        hraBorderPane.setBottom(spodniBox);
    }

    private void pripravSpodniPanel() {
        spodniBox = new HBox();
        spodniBox.setAlignment(Pos.CENTER);
        spodniBox.getChildren().addAll(zadejPrikazLabel, prikazovePole);
    }

    private void pripravPrikazovePole() {
        prikazovePole = new TextField();
        prikazovePole.setOnAction(event -> {
            String prikaz = prikazovePole.getText();
            konzole.appendText("\n" + prikaz + "\n");
            prikazovePole.setText("");
            String odpovedHry = hra.zpracujPrikaz(prikaz);
            konzole.appendText("\n" + odpovedHry + "\n");

            if (hra.konecHry()) {
                prikazovePole.setEditable(false);
            }
        });

        zadejPrikazLabel = new Label("Zadej příkaz: ");
        zadejPrikazLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
    }

    private void pripravKonzoli() {
        konzole = new TextArea();
        konzole.setText(hra.vratUvitani());
        konzole.setEditable(false);
    }
}

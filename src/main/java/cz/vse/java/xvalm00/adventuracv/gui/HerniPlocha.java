package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.util.Observer;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

public class HerniPlocha implements Observer {

    private final AnchorPane anchorPane = new AnchorPane();
    private HerniPlan herniPlan;
    private ImageView viewKarkulky;

    public HerniPlocha(HerniPlan plan, ImageView viewKarkulky){
        this.herniPlan = plan;
        this.viewKarkulky = viewKarkulky;

        init();
        herniPlan.registerObserver(this);
    }

    private void init() {
        ImageView herniPlanImageView = new ImageView(new Image(HerniPlocha.class.getResourceAsStream
                ("/zdroje/herniPlan.png"), 400, 250, false, false));

        nastavPoziciHrace();

        anchorPane.getChildren().addAll(herniPlanImageView, viewKarkulky);
    }

    public AnchorPane getAnchorPane() {
        return anchorPane;
    }

    @Override
    public void update() {
        nastavPoziciHrace();
    }

    private void nastavPoziciHrace() {
        AnchorPane.setTopAnchor(viewKarkulky, herniPlan.getAktualniProstor().getPosTop());
        AnchorPane.setLeftAnchor(viewKarkulky, herniPlan.getAktualniProstor().getPosLeft());
    }

    public void novaHra(HerniPlan herniPlan) {
        this.herniPlan = herniPlan;
        update();
    }
}

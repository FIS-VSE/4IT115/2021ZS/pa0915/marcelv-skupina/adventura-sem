package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.Prostor;
import cz.vse.java.xvalm00.adventuracv.util.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

public class PanelVychodu implements Observer {

    HerniPlan herniPlan;
    ListView<String> listView = new ListView<>();
    ObservableList<String> vychody = FXCollections.observableArrayList();


    public PanelVychodu(HerniPlan herniPlan) {
        this.herniPlan = herniPlan;

        init();
        herniPlan.registerObserver(this);
    }

    private void init() {
        nactiVychodyAktualnihoProstoru();

        listView.setItems(vychody);
        listView.setPrefWidth(100);
    }

    public ListView<String> getListView() {
        return listView;
    }

    @Override
    public void update() {
        nactiVychodyAktualnihoProstoru();
    }

    private void nactiVychodyAktualnihoProstoru() {
        vychody.clear();
        Prostor aktualniProstor = herniPlan.getAktualniProstor();
        for (Prostor prostor : aktualniProstor.getVychody()) {
            vychody.add(prostor.getNazev());
        }
    }

    public void novaHra(HerniPlan herniPlan) {
        this.herniPlan = herniPlan;
        update();
    }
}

package cz.vse.java.xvalm00.adventuracv.util;

/**
 * Návrhový vzor Observer (Gang of Four) - Teorie
 *
 * Problém:
 *
 * Na stavu jednoho objektu závisí jiné objekty. Jakmile se změní stav tohoto objektu, všechny na něm závislé je třeba informovat
 *
 * Objekty by měly být volně vázané – to znamená, že objekt, který změní svůj stav a bude o tom informovat ostatní, by neměl být závislý na jejich vnitřním uspořádání.
 *
 * Objekty, které budou přijímat informace o změně stavu, nemusí být předem známy.
 *
 * Řešení:
 *
 * Objekt, který mění svůj stav, představuje subjekt. Subjekt posílá informaci těm, kteří se u něj zaregistrovali jako pozorovatelé (observer).
 *
 * Každý pozorovatel musí mít definovánu speciální metodu update, kterou subjekt volá.
 */
public interface Observer {

    /**
     * Reakce na pozorovanou změnu
     */
    void update();

}
